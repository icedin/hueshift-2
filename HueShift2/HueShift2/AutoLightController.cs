﻿using HueShift2.Configuration;
using HueShift2.Configuration.Model;
using HueShift2.Interfaces;
using HueShift2.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Q42.HueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HueShift2
{
    public class AutoLightController: ILightController
    {
        private readonly HueShiftMode mode;
        private readonly ILogger<AutoLightController> logger;
        private readonly IOptionsMonitor<HueShiftOptions> appOptionsDelegate;

        private IHueClientManager clientManager;
        private ILightManager lightManager;
        private ILocalHueClient client;
        private IAutoColourProvider colourProvider;
   

        public AutoLightController(ILogger<AutoLightController> logger, IOptionsMonitor<HueShiftOptions> appOptionsDelegate, IOptionsMonitor<CustomScheduleOptions> scheduleOptionsDelegate,
            IHueClientManager clientManager, ILightManager lightManager, ILocalHueClient client, IAutoColourProvider colourProvider)
        {
            this.mode = HueShiftMode.Auto;
            this.logger = logger;
            this.appOptionsDelegate = appOptionsDelegate;
            this.clientManager = clientManager;
            this.lightManager = lightManager;
            this.client = client;
            this.colourProvider = colourProvider;
        }

        private async Task<ControlParameters> Setup()
        {
            await clientManager.AssertConnected();
            var lightsToExclude = appOptionsDelegate.CurrentValue.LightsToExclude;
            var lightsToControl = lightManager.GetLightsToControl();
            return new ControlParameters(lightsToControl);
        }

        public async Task Execute()
        {
            logger.LogInformation("Executing automatic light control...");
            var controlParameters = await Setup();
            var targetColourTemperature = colourProvider.TargetColour(controlParameters.CurrentTime);

            return;
        }

        public HueShiftMode Mode()
        {
            return mode;
        }
    }
}
