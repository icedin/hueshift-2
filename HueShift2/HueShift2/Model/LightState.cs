﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HueShift2.Model
{
    public class LightState : IEquatable<LightState>
    {
        public LightPowerState PowerState { get; private set; }
        public double? Brightness { get; private set; }

        public readonly string Colour;
        public readonly int ColourTemperature;

        public LightState(bool isOn, double brightness, string colour, int colourTemperature)
        {
            Brightness = brightness;
            Colour = colour;
            ColourTemperature = colourTemperature;
        }

        public override bool Equals(object obj)
        {
            return obj is LightState && Equals((LightState)obj);
        }

        public bool Equals(LightState other)
        {
            return IsOn == other.IsOn &&
                   Brightness == other.Brightness &&
                   Colour == other.Colour &&
                   ColourTemperature == other.ColourTemperature;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Brightness, Colour, ColourTemperature);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
