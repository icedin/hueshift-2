﻿using Q42.HueApi;
using System;
using System.Collections.Generic;
using System.Text;

namespace HueShift2.Model
{
    public class HueShiftLight
    {
        public readonly string Id;

        public LightControlState ControlState {get; set;}
        public LightState State { get; private set; }

        public HueShiftLight(Light light)
        {
            Id = light.Id;
            ControlState = LightControlState.HueShift;
        }

        public void Exclude()
        {
            this.ControlState = LightControlState.Excluded;
        }
    }
}
