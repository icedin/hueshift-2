﻿using Q42.HueApi;
using System;
using System.Collections.Generic;
using System.Text;

namespace HueShift2.Model
{
    public class ControlParameters
    {
        public readonly DateTime CurrentTime;
        public readonly Light[] LightsToControl;

        public ControlParameters(Light[] lightsToControl)
        {
            CurrentTime = DateTime.Now;
            LightsToControl = lightsToControl;
        }
    }
}
