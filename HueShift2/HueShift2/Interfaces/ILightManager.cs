﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HueShift2.Interfaces
{
    public interface ILightManager
    {
        public Task DiscoverLightsOnNetwork();
        public void ListAllLights();
        public void ListExcludedLights();
    }
}
