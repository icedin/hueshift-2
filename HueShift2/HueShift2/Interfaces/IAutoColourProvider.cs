﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HueShift2.Interfaces
{
    public interface IAutoColourProvider
    {
        public int TargetColour(DateTime currentTime);
    }
}
