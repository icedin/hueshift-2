﻿using HueShift2.Configuration.Model;
using HueShift2.Interfaces;
using HueShift2.Model;
using Innovative.SolarCalculator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using TimeZoneConverter;

namespace HueShift2
{
    public class AutoColourProvider : IAutoColourProvider
    {
        private readonly ILogger<AutoColourProvider> logger;
        private readonly IConfiguration configuration; 
        private readonly IOptionsMonitor<HueShiftOptions> appOptionsDelegate;

        public AutoColourProvider(ILogger<AutoColourProvider> logger, IConfiguration configuration, IOptionsMonitor<HueShiftOptions> appOptionsDelegate)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.appOptionsDelegate = appOptionsDelegate;
        }

        private TimeZoneInfo DetermineTimeZoneId(string timeZone)
        {
            try
            {
                var isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                if (isWindows)
                {
                    var windowsId = TZConvert.IanaToWindows(timeZone);
                    return TimeZoneInfo.FindSystemTimeZoneById(windowsId);
                }
                var isLinux = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
                if (isLinux)
                {
                    return TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                }
                throw new PlatformNotSupportedException();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"HueShift-2 does not support OSX");
                throw;
            }
        }

        private AutoTransitionTimes DetermineTransitionTimes()
        {
            var geolocation = appOptionsDelegate.CurrentValue.Geolocation;
            var tz = DetermineTimeZoneId(geolocation.TimeZone);
            var solarTimes = new SolarTimes(DateTime.Now, geolocation.Latitude, geolocation.Longitude);
            var sunrise = TimeZoneInfo.ConvertTimeFromUtc(solarTimes.Sunrise.ToUniversalTime(), tz);
            var sunset = TimeZoneInfo.ConvertTimeFromUtc(solarTimes.Sunset.ToUniversalTime(), tz);
            var midnight = sunrise.Date;
            var autoTransitionTimeLimits = appOptionsDelegate.CurrentValue.AutoTransitionTimeLimits;
            return new AutoTransitionTimes(
                sunrise.Clamp(midnight + autoTransitionTimeLimits.SunriseLower, midnight + autoTransitionTimeLimits.SunriseUpper),
                sunset.Clamp(midnight + autoTransitionTimeLimits.SunsetLower, midnight + autoTransitionTimeLimits.SunsetUpper)
            );
        }

        public int TargetColour(DateTime currentTime)
        {
            var transitionTimes = DetermineTransitionTimes();
            var colourTemperatures = appOptionsDelegate.CurrentValue.ColourTemperature;
            return (currentTime <= transitionTimes.Day || currentTime >= transitionTimes.Night) ? colourTemperatures.Night : colourTemperatures.Day;
        }
    }
}
