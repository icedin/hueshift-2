﻿using HueShift2.Configuration.Model;
using HueShift2.Interfaces;
using HueShift2.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Q42.HueApi;
using Q42.HueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HueShift2
{
    public class LightManager : ILightManager
    {
        private ILogger<LightManager> logger;
        private IOptionsMonitor<HueShiftOptions> appOptionsDelegate;

        private IHueClientManager clientManager;
        private ILocalHueClient client;

        private List<Light> lightsOnNetwork;

        public List<HueShiftLight> hueShiftLights;


        public LightManager(ILogger<LightManager> logger, IOptionsMonitor<HueShiftOptions> appOptionsDelegate, IHueClientManager clientManager, ILocalHueClient client)
        {
            this.logger = logger;
            this.appOptionsDelegate = appOptionsDelegate;
            this.clientManager = clientManager;
            this.client = client;
            this.lightsOnNetwork = new List<Light>();
        }



        private async Task RefreshLights()
        {
            await clientManager.AssertConnected();
            var discoveredLights = (await client.GetLightsAsync()).ToList();
            logger.LogInformation($"Discovered {discoveredLights.Count - lightsOnNetwork.Count} new lights on network.");
            lightsOnNetwork = discoveredLights;
            this.hueShiftLights = discoveredLights.Select(x => new HueShiftLight(x)).ToList();
            foreach (var light in hueShiftLights)
            {
                if (appOptionsDelegate.CurrentValue.LightsToExclude.Any(l => l == light.Id))
                {
                    light.Exclude();
                }
            }
            //need to establish if any light has changed and gone under manual control here...
        }
        
        public async Task<IList<HueShiftLight>> GetLightsToControl()
        {
            await RefreshLights();
            return hueShiftLights.Where(x => x.ControlState != LightControlState.Excluded).ToList();
        }

        public async Task DiscoverLightsOnNetwork()
        {
            await RefreshLights();
            ListAllLights();
            ListExcludedLights();
        }








        public void ListAllLights()
        {
            var logMessage = "Lights on network: \n";
            foreach (var light in lightsOnNetwork)
            {
                logMessage += $"ID: {light.Id, -4} Name: {light.Name, -20} ModelID: {light.ModelId, -10} ProductID: {light.ProductId, -10} \n";
            }
            logger.LogInformation(logMessage);
        }

        public void ListExcludedLights()
        {
            var logMessage = "Excluded lights: \n";
            var ids = this.lightsOnNetwork.ToDictionary(x => x.Id, x => x);
            foreach (var id in appOptionsDelegate.CurrentValue.LightsToExclude)
            {
                logMessage += $"ID: {id} Name: {ids[id].Name} \n";
            }
            logger.LogInformation(logMessage);
        }
    }
}
