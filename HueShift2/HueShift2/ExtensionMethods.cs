﻿using HueShift2.Configuration;
using HueShift2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HueShift2
{
    public static class ExtensionMethods
    {
        public static DateTime Clamp(this DateTime dt, DateTime min, DateTime max)
        {
            if (dt <= min) return min;
            if (dt >= max) return max;
            return dt;
        }
    }
}
