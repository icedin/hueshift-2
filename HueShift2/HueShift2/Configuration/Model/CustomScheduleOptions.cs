﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HueShift2.Configuration.Model
{
    public class CustomScheduleOptions
    {
        public Transition[] Programme { get; set; } = Array.Empty<Transition>();

        public CustomScheduleOptions()
        {

        }

        public void SetDefaults()
        {
            Programme = Array.Empty<Transition>();
            return;
        }
    }


}
