﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HueShift2.Configuration.Model
{
    public class Transition
    {
        public DateTime TransitionTime;
        public LightState LightState;
    }
}
