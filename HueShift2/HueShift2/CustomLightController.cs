﻿using HueShift2.Configuration;
using HueShift2.Configuration.Model;
using HueShift2.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Q42.HueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HueShift2
{
    public class CustomLightController : ILightController
    {
        private readonly HueShiftMode mode;
        private readonly ILogger<AutoLightController> logger;
        private readonly IOptionsMonitor<HueShiftOptions> appOptionsDelegate;

        private IHueClientManager clientManager;
        private ILocalHueClient client;

        public CustomLightController(ILogger<AutoLightController> logger, IOptionsMonitor<HueShiftOptions> appOptionsDelegate, IOptionsMonitor<CustomScheduleOptions> scheduleOptionsDelegate,
            IHueClientManager clientManager, ILocalHueClient client, IAutoColourProvider colourProvider)
        {
            this.mode = HueShiftMode.Auto;
            this.logger = logger;
            this.appOptionsDelegate = appOptionsDelegate;
            this.clientManager = clientManager;
            this.client = client;
        }

        public async Task Execute()
        {
            throw new NotImplementedException();
        }

        public HueShiftMode Mode()
        {
            return mode;
        }
    }
}
